#include <stdio.h>
#include "Preproc_Cfg.h"
#include "preproc.h"

void FUNC_PREPROC_TASK1_ID(void)
{
   printf("PREPROC_TASK1_ID" " = %d\n", (3U));
}
void FUNC_PREPROC_TASK2_ID(void)
{
   printf("PREPROC_TASK2_ID" " = %d\n", (2U));
}
void FUNC_PREPROC_TASK3_ID(void)
{
   printf("PREPROC_TASK3_ID" " = %d\n", (1U));
}



int main()
{
   FUNC_PREPROC_TASK1_ID();
   FUNC_PREPROC_TASK2_ID();
   FUNC_PREPROC_TASK3_ID();
   return 0;
}
